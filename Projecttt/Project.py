import os
from tkinter import  *
from tkinter import filedialog
from tkinter import messagebox

d = os.getcwd() #เป็นคำสั่งที่ใช้แสดงที่ตั้งปัจจุบันใน Python
pathspam = os.path.join(d, "DatasetSMS\Train\spam");
pathham = os.path.join(d, "DatasetSMS\Train\ham");
pathtestspam = os.path.join(d, "DatasetSMS\Test\spam");
pathtestham= os.path.join(d, "DatasetSMS\Test\ham");


#-----------------เก็บจำนวนไฟล์ในแต่ละตัว-------------------------#
countfilespam = len(os.listdir(pathspam));
countfileham = len(os.listdir(pathham));
counttestspam =len(os.listdir(pathtestspam));
counttestham =len(os.listdir(pathtestham));

#------------------------------------------#
stopwordfile = open("stopword.txt", "r");  #เปิดไฟล์
stopword = stopwordfile.read().splitlines() #เก็บทีละบรรทัด
valueAlldata=countfileham+countfilespam; #เก็บจำนวนไฟล์เทรนด์ทั้งหมด
checkaccuracy = False   #เช็คค่าความแม่นยำ
dicaccuracy = {}
counttestAll=counttestspam+counttestham; #เก็บจำนวนไฟล์เทสทั้งหมด
alltestcount=0.0
countspam =-1;
countnonespam=-1;
check = 0.0
filenamedataSetSpam = 'PredictdatasetSpam.txt'
filenamedataSetNoneSpam = 'PredictdatasetNoneSpam.txt'
def datasetspam(filenames,path='text',length=1,checkaccuracy=False):
    litetextresult = ([])
    global check
    for i in range(1,length+1):
        if str(filenames) is "Spam"or str(filenames) is "NoneSpam":
            name= filenames+' ('+str(i)+').txt'
            filename = os.path.join(path,name);
            print(filename)
        else:
            filename=filenames


        text = open(filename,"r");
        textsuc=text.read().replace('"',"").replace('.',"").replace(')',"").replace('(',"").replace('?', "").replace('!',"").replace('*',"").replace('-',"").replace('/',"").replace(':',"").lower().replace('',"").splitlines();
        countline=len(textsuc)

        litetexts = []
        for j in range(countline):
            texts=textsuc[j].lower().split(" ");
            for k in (texts):
                for stop in stopword:
                    if (stop in texts):
                       texts.remove(stop);
            litetexts+=texts;

        if checkaccuracy == True:
            Testhamaccuracy = Test([litetexts],Popnonspam,countfileham)
            Testspamaccuracy = Test([litetexts], Popspam, countfilespam)
            Testham = summarize(Testhamaccuracy);
            Testspam = summarize(Testspamaccuracy);
            resultdatast = compare(Testspam, Testham);

            dicaccuracy.update({filenames+' '+str(i):resultdatast});
            print(litetexts)
            print(filenames ,'\t', resultdatast);
            print("spam : ", Testspam);
            print("Nonespam : ", Testham);
            print("-------------------------");
        litetextresult.append(litetexts);
        text.close()

    if checkaccuracy == True:

        global  countnonespam,countspam
        if str(filenames)is"NoneSpam":
            countnonespam=list(dicaccuracy.values()).count("NoneSpam");
            check += 1;
        elif str(filenames)is"Spam":
            countspam = list(dicaccuracy.values()).count("Spam");
            check+=1;
        dicaccuracy.clear()

    if check==2:
        print("final spam ",countspam)
        print("final nonspam ", countnonespam)
        print("Allfile ", counttestAll)
        global accuracyis
        accuracyis = countnonespam + countspam;
        print("final",(accuracyis / counttestAll) * 100)
        showresult.config(text='All file test : '+str(counttestAll)+'\nAll spam files: '+str(counttestspam)+' Result is '+str(countspam)+
                               '\nAll NoneSpam files: '+str(counttestham)+' Result is '+str(countnonespam))
        showresult2.config(text='Accuracy: %.2f %%'%((accuracyis / counttestAll) * 100))
        check=0

    else:
        return litetextresult
        litetextresult.clear()


def wordunion(spam,nonspam):
    textU=spam[0]
    for i in spam:
        textU=set(textU).union(set(i))
    for i in nonspam:
        textU=set(textU).union(set(i))
    word={}

    if '' in textU:
        textU.pop();

    for i in textU:
        word.update({i:[]})
    return word

def setdatainword(data,word):
    for key in word:
        for j in data:
            if key in j:
                word.get(key).append(list(j).count(key));
            else :
                word.get(key).append(0);
    return  word
def NavieBayes(table, result):
    Pop = {}
    n=0.0;
    nlist=[];
    for key,val in table.items():
        nlist.append(val)
    for val in nlist:
        for i in val:
           n+=i
    for key, value in table.items():
        Pop.update({key: ((len(value)-value.count(0))+1) / (n+result)});
    return Pop

def Test(fileT, Pop, count):
    value = []
    print("file : ",fileT)
    for key in fileT:
        for word in key:
            if word in Pop:
                value.append(Pop.get(word) / (count))
    value.append(count / valueAlldata)
    print("value : ", value)
    return value

def summarize(datas):
    result = 1.0
    for i in datas:
        if i != 0:
            result *= i;
    return result

def compare(spam, nonspam):

    if spam >= nonspam:
        return "Spam"
    elif spam<nonspam:
        return "NoneSpam"
    else :
        return "I'don't know"
def choose():
    #text.config(state=NORMAL)
    text.delete('1.0',END)
    pathchoose = filedialog.askopenfilename();
    path=''
    for i in pathchoose:
        path +=i;
    s=open(path.replace('/','\\'),'r');
    sw=s.read().splitlines();
    text.insert(END,str(sw).replace("[","").replace("]",""))
    #text.config(state=DISABLED)
    Showfile.config(text=path.replace('/','\\'))
    global filetest
    filetest = datasetspam(path.replace('/','\\'));

def accuracy():

    text.delete('1.0', END)
    text.insert(END, "Processing...")
    messagebox.showinfo("Alert", "Accuracy ?")
    global ac_spam,ac_nonspam,ts,t
    checkaccuracy=TRUE
    global check
    check=0
    dicaccuracy.clear()
    ac_spam = datasetspam("Spam", pathtestspam, counttestspam,checkaccuracy)
    ac_nonspam = datasetspam("NoneSpam", pathtestham, counttestham,checkaccuracy)
    text.delete('1.0', END)
    text.insert(END, "Successful...")

def filter():
    datawordspam = Test(filetest, Popspam, countfilespam)
    datawordham = Test(filetest, Popnonspam, countfileham)
    spamcom = summarize(datawordspam);
    nonespam = summarize(datawordham);
    print("spam : ", spamcom);
    print("Nonespam : ", nonespam);
    resultdata = compare(spamcom, nonespam);
    print("result : ", resultdata)
    showresult.config(text="File : "+resultdata)
def readfiledataset(filedataset):
    tables ={}
    readpre = open(filedataset, 'r');
    pre = readpre.read().splitlines();
    readpre.close()
    for i in pre:
        strs = i.split("|")
        tables.update({str(strs[0]): []})
        for val in strs[1].replace(" ", "").replace(",", "").replace("[", "").replace("]", ""):
            tables.get(strs[0]).append(int(val))
    return tables

def createDataset():
    text.delete('1.0', END)
    text.insert(END, "Pre-processing")
    messagebox.showinfo("Alert", "Creating Dataset")
    text.delete('1.0', END)
    text.insert(END, "Creating Dataset")
    global listspam, Popnonspam, Popspam, listnonspam
    listspam = datasetspam("Spam", pathspam, countfilespam)
    listnonspam = datasetspam("NoneSpam", pathham, countfileham)
    wordspam = wordunion(listspam, listnonspam)

    for key in wordspam:
        for j in listspam:
            if key in j:
                wordspam.get(key).append(list(j).count(key));
            else:
                wordspam.get(key).append(0);
    for key in wordspam:
        for j in listnonspam:
            if key in j:
                wordspam.get(key).append(list(j).count(key));
            else:
                wordspam.get(key).append(0);
    wordall = {}
    for kek, val in wordspam.items():
        s = 0.0
        for i in val:
            s += i
        wordall.update({kek: s})

    spamwordsuccess = {}
    nonespamwordsuccess = {}
    for key, val in wordall.items():
        if val >= 10:
            spamwordsuccess.update({key: []})
            nonespamwordsuccess.update({key: []})
    print("Atb : ",len(spamwordsuccess))
    tablespam = setdatainword(listspam, spamwordsuccess)
    tablenonspam = setdatainword(listnonspam, nonespamwordsuccess)

    predictdataset = open(filenamedataSetSpam, 'w+')
    for key, val in tablespam.items():
        predictdataset.writelines(str(key) + '|' + str(val) + '\n')
    predictdataset.close()
    print("Created "+filenamedataSetSpam)

    predictdataset = open(filenamedataSetNoneSpam, 'w+')
    for key, val in tablenonspam.items():
        predictdataset.writelines(str(key) + '|' + str(val) + '\n')
    predictdataset.close()
    print("Created " + filenamedataSetNoneSpam)
    text.delete('1.0', END)
    text.insert(END, "Dataset is ready...")
    allattribute = len(tablespam);
    Popnonspam = NavieBayes(tablenonspam, allattribute);
    Popspam = NavieBayes(tablespam, allattribute);

def main ():

    global Popnonspam, Popspam
    try:
        text.insert(END, "Dataset is ready...")
        tablespamRead=readfiledataset(filenamedataSetSpam);
        tablenonspamRead = readfiledataset(filenamedataSetNoneSpam);
        allattribute = len(tablespamRead);
        Popnonspam = NavieBayes(tablenonspamRead, allattribute);
        Popspam = NavieBayes(tablespamRead, allattribute);
    except:
        text.insert(END,"Click Pre-processing")


gui = Tk();
gui.geometry('1480x780');
gui.title('Filter Spam mail')

getter_text= StringVar()

rightF=Frame(gui,width=20,height=20);
rightF.grid(row=1,column=1,padx=40,pady=30);
text= Text(rightF,relief=GROOVE,padx=20);

text.config(font=('Angsananew',18,'normal'))
text.config(width=60,height=12)
text.pack();

frame0 = Frame(gui,width=250,bd=2,relief=GROOVE);
frame0.grid(row=2, column=1)
brown= Button(frame0,text="Brown",width=10,height=2,padx=10,pady=2,relief=RAISED,command=choose);
brown.pack(side=RIGHT);
brown.config(font=('Angsananew',10,'normal'))
Showfile = Label(frame0,text="Filename",padx=20);
Showfile.pack(side=LEFT);
Showfile.config(font=('Angsananew',10,'normal'))

filter=Button(gui,text="Filter",width=20,height=1,command=filter);
filter.config(font=('Angsananew',10,'normal'))
filter.grid(row=3,column=1,pady=20)

prepro=Button(gui,text="Pre-processing",width=20,height=1,command=createDataset);
prepro.config(font=('Angsananew',10,'normal'))
prepro.grid(row=2,column=2,pady=20)


framerigth = Frame(gui,width=250,height=100,bd=2,relief=GROOVE);
framerigth.grid(row=4, column=1,padx=30,pady=2)
showresult=Label(framerigth,text="",padx=20);
showresult.config(font=('Angsananew',18,'normal'))
showresult.pack();

frameac = Frame(gui,width=250,bd=2,relief=GROOVE);
frameac.grid(row=5, column=1,pady=20)
accuracy= Button(frameac,text="Accuracy",width=10,height=2,padx=10,pady=2,relief=RAISED,command=accuracy);
accuracy.pack(side=RIGHT);
accuracy.config(font=('Angsananew',10,'normal'))
showresult2 = Label(frameac,text="Accuracy : ",padx=20);
showresult2.pack(side=LEFT);
showresult2.config(font=('Angsananew',14,'normal'))

main()
mainloop()
